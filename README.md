# Robotic Surgical Propositional Bank



## Abstract

Robot-Assisted minimally invasive robotic surgery is the gold standard for the surgical treatment of many pathological
conditions, and several manuals and academic papers describe how to perform these interventions. These high-quality,
often peer-reviewed texts are the main study resource for medical personnel and consequently contain essential procedural
domain-specific knowledge. The procedural knowledge therein described could be extracted, e.g., on the basis of semantic
parsing models, and used to develop clinical decision support systems or even automation methods for some procedure’s steps.
However, natural language understanding algorithms such as, for instance, semantic role labelers have lower efficacy and
coverage issues when applied to domain others than those they are typically trained on (i.e., newswire text). To overcome this
problem, starting from PropBank frames, we propose a new linguistic resource specific to the robotic-surgery domain, named
Robotic Surgery Procedural Framebank (RSPF). We extract from robotic-surgical texts verbs and nouns that describe surgical
actions and extend PropBank frames by adding any of new lemmas, frames or role sets required to cover missing lemmas,
specific frame describing the surgical significance, or new semantic roles used in procedural surgical language. Our resource
can be used to annotate corpora in the surgical domain to train and evaluate Semantic Role Labeling
(SRL) systems in a challenging fine-grained domain setting.

## Resource presentation
Our work represents an adaptation of the latest release (version 3.1) of PropBank (Palmer et al., 2005) to
the robotic-surgical domain. We argue this adaptation is necessary for extracting meaningful robotic-surgical
procedural knowledge, as the application domain is substantially different than the general English considered in PropBank. The overall development of the Robotic Surgery Procedural Propbank can be divided into two parts, namely the creation of a lexicon of frames files and the annotated corpus.  In the folder "Robotic_Surgery_Procedural_FrameBank" we address the
first part. 

You can ask for the annotated corpus with these domain-specific frames by agreeing to the license by filling out [this](https://docs.google.com/forms/d/e/1FAIpQLSdh2BSKmwOyLDJ8Eak3ZIV5Y8q7x_2HipiKv2MdiiKRKHQm7g/viewform?usp=pp_url) form.

You can find a list of added/modified frames [here](https://gitlab.com/altairLab/robotic-surgery-propositional-bank/-/blob/main/added_or_modified_frames.txt).


## Contact us
For any question about the resource, if you want to collaborate, or if you find some errors, please contact us at:

<em>"marco[dot]bombieri_01[at]univr[dot]it"</em>

## Cite us
If you use this resource please cite the paper: 
<em>
<br />
<br />
@InProceedings{bombieri_et_al_lrec-2022,<br />
  author = {Marco Bombieri and Marco Rospocher and Simone Paolo Ponzetto and Paolo Fiorini},<br />
  title = "{The Robotic Surgery Procedural Framebank}",<br />
  booktitle = {Proceedings of the Thirteenth International Conference on Language Resources and Evaluation (LREC 2022)},<br />
  year = {2022},<br />
  month = {June 21-23, 2022},<br />
  address = {Marseille, France},<br />
  publisher = {European Language Resources Association (ELRA)},<br />
  isbn = {979-10-95546-72-6},<br />
  language = {english}<br />
  }
</em>

